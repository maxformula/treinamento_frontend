### Olá novos Maxters!


Nos últimos dias vocês tiveram várias palestras sobre Vue.js.
Agora, chegou a hora de pôr o conhecimento adquirido a prova.

Para isso vamos criar um <b>todo list</b>,
um projeto simples onde terão que utilizar conhecimentos como:

* renderização de listas
* criação de componentes
* comunicação de eventos entre componentes
* transmissão de dados por atributos personalizáveis (props)

Este é [Protótipo](https://www.figma.com/file/v6cuANLlY50BTMpSc7XHMT/Teste-estagi%C3%A1rios?node-id=0%3A286) que vocês devem seguir.

Boa sorte!